# Tutamen

Tutamen is an alternative library and helper for Polkit agents that among other things, supports simultaneous use of multiple PAM authenticators.

*Tutamen is Latin for armour, protection, defence.*
