/*
    SPDX-FileCopyrightText: 2023 Janet Blackquill <uhhadd@gmail.com>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#pragma once

#include "pamauthenticator.h"
#include <QObject>
#include <memory>

class PamAuthenticators : public QObject
{
    Q_OBJECT

    // these properties delegate to the interactive authenticator
    Q_PROPERTY(bool busy READ isBusy NOTIFY busyChanged)

    Q_PROPERTY(QString prompt READ getPrompt NOTIFY prompt)
    Q_PROPERTY(QString promptForSecret READ getPromptForSecret NOTIFY promptForSecret)

    Q_PROPERTY(QString infoMessage READ getInfoMessage NOTIFY infoMessage)
    Q_PROPERTY(QString errorMessage READ getErrorMessage NOTIFY errorMessage)

    // this property true if any of the authenticators' unlocked properties are true
    Q_PROPERTY(bool unlocked READ isUnlocked NOTIFY succeeded)

    // this property is a sum of the noninteractive authenticators' flags
    Q_PROPERTY(PamAuthenticator::NoninteractiveAuthenticatorTypes authenticatorTypes READ authenticatorTypes NOTIFY authenticatorTypesChanged)

    Q_PROPERTY(AuthenticatorsState state READ state NOTIFY stateChanged)

public:
    PamAuthenticators(std::unique_ptr<PamAuthenticator> &&interactive,
                      std::vector<std::unique_ptr<PamAuthenticator>> &&noninteractive,
                      QObject *parent = nullptr);
    ~PamAuthenticators() override;

    enum AuthenticatorsState: uint32_t {
        Idle = 0,
        Authenticating = 1,
        Failed = 2,
    };
    Q_ENUM(AuthenticatorsState)

    AuthenticatorsState state() const;
    Q_SIGNAL void stateChanged();
    Q_INVOKABLE void startAuthenticating();
    Q_INVOKABLE void stopAuthenticating();

    // these properties delegate to the interactive authenticator
    bool isBusy() const;
    Q_SIGNAL void busyChanged();

    QString getPrompt() const;
    Q_SIGNAL void prompt(const QString &msg);
    QString getPromptForSecret() const;
    Q_SIGNAL void promptForSecret(const QString &msg);
    QString getInfoMessage() const;
    Q_SIGNAL void infoMessage(const QString &msg);
    QString getErrorMessage() const;
    Q_SIGNAL void errorMessage(const QString &msg);

    // these delegate to interactive authenticator
    Q_INVOKABLE void respond(const QByteArray &response);
    Q_INVOKABLE void cancel();

    // this property is true if any of the authenticators' unlocked properties are true
    bool isUnlocked() const;
    Q_SIGNAL void succeeded();

    PamAuthenticator::NoninteractiveAuthenticatorTypes authenticatorTypes() const;
    Q_SIGNAL void authenticatorTypesChanged();

    Q_SIGNAL void failed(PamAuthenticator::NoninteractiveAuthenticatorTypes what, PamAuthenticator *authenticator);
    Q_SIGNAL void noninteractiveError(PamAuthenticator::NoninteractiveAuthenticatorTypes what, PamAuthenticator *authenticator);
    Q_SIGNAL void noninteractiveInfo(PamAuthenticator::NoninteractiveAuthenticatorTypes what, PamAuthenticator *authenticator);

private:
    struct Private;
    QScopedPointer<Private> d;

    // convenience internal function for setting state,
    // should not be exposed to outsiders
    void setState(AuthenticatorsState state);
};
