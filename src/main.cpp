/*
    SPDX-FileCopyrightText: 2023 Janet Blackquill <uhhadd@gmail.com>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#include <KUser>
#include <PolkitQt1/Authority>
#include <QCoreApplication>
#include <QFile>
#include <QSocketNotifier>
#include <polkitqt1-identity.h>

#include "pamauthenticator.h"
#include "pamauthenticators.h"
#include "tutamen_helper_logging.h"

enum AuthenticatorKind : uint32_t {
    Primary = 0,
    Fingerprint = 1 << 0,
    Smartcard = 1 << 2,
};

int main(int argc, char* argv[])
{
    QCoreApplication::setSetuidAllowed(true);
    QCoreApplication app(argc, argv);

    if (geteuid() != 0) {
        qCWarning(TUTAMEN_HELPER) << "Helper must be suid; expected effective UID 0 but got" << geteuid();
        return -1;
    }

    if (app.arguments().count() != 2)
    {
        qCWarning(TUTAMEN_HELPER) << "Inappropriate use of helper, wrong number of arguments";
        return -1;
    }

    QFile stdin;
    if (!stdin.open(0, QFile::ReadOnly | QFile::Unbuffered)) {
        qCWarning(TUTAMEN_HELPER) << "Helper failed to open stdin with QFile";
        return -1;
    }
    QFile stdout;
    if (!stdout.open(1, QFile::WriteOnly | QFile::Unbuffered)) {
        qCWarning(TUTAMEN_HELPER) << "Helper failed to open stdout with QFile";
        return -1;
    }
    QDataStream inputStream(&stdin);
    QDataStream outputStream(&stdout);

    auto identity = PolkitQt1::Identity::fromString(app.arguments().at(1));
    if (!identity.isValid())
    {
        qCWarning(TUTAMEN_HELPER) << "Inappropriate use of helper, invalid identity";
        return -1;
    }
    auto userIdentity = identity.toUnixUserIdentity();
    if (!userIdentity.isValid())
    {
        qCWarning(TUTAMEN_HELPER) << "Inappropriate use of helper, identity is not user identity";
        return -1;
    }
    const auto kuser = KUser(userIdentity.uid());
    if (!kuser.isValid())
    {
        qCWarning(TUTAMEN_HELPER) << "Inappropriate use of helper, user identity is invalid";
        return -1;
    }
    const auto loginName = kuser.loginName();
    const auto cookie = QString::fromLocal8Bit(stdin.readLine().trimmed());

    auto interactive = std::make_unique<PamAuthenticator>(QStringLiteral("kde"), loginName);
    std::vector<std::unique_ptr<PamAuthenticator>> noninteractive;
    noninteractive.push_back(std::make_unique<PamAuthenticator>(QStringLiteral("kde-fingerprint"), loginName, PamAuthenticator::Fingerprint));
    noninteractive.push_back(std::make_unique<PamAuthenticator>(QStringLiteral("kde-smartcard"), loginName, PamAuthenticator::Smartcard));

    auto authenticators = new PamAuthenticators(std::move(interactive), std::move(noninteractive), &app);

    QSocketNotifier notifier(0, QSocketNotifier::Read);
    QObject::connect(&notifier, &QSocketNotifier::activated, &app, [&] {
        QString request;
        inputStream >> request;

        if (request == QStringLiteral("startAuthenticating")) {
            qCDebug(TUTAMEN_HELPER) << "Helper received startAuthenticating request";
            authenticators->startAuthenticating();
        } else if (request == QStringLiteral("stopAuthenticating")) {
            qCDebug(TUTAMEN_HELPER) << "Helper received stopAuthenticating request";
            authenticators->stopAuthenticating();
        } else if (request == QStringLiteral("respond")) {
            qCDebug(TUTAMEN_HELPER) << "Helper received respond request";
            QByteArray response;
            inputStream >> response;
            authenticators->respond(response);
        } else {
            qCWarning(TUTAMEN_HELPER) << "Inappropriate use of helper, unknown request" << request;
            app.exit();
        }
    });

    QObject::connect(authenticators, &PamAuthenticators::stateChanged, &app, [&] {
        qCDebug(TUTAMEN_HELPER) << "Helper is sending state signal" << authenticators->state();
        outputStream << QStringLiteral("state") << authenticators->state();
    });
    QObject::connect(authenticators, &PamAuthenticators::prompt, &app, [&](const QString &msg) {
        qCDebug(TUTAMEN_HELPER) << "Helper is sending prompt signal w/ msg" << msg;
        outputStream << QStringLiteral("prompt") << msg;
    });
    QObject::connect(authenticators, &PamAuthenticators::promptForSecret, &app, [&](const QString &msg) {
        qCDebug(TUTAMEN_HELPER) << "Helper is sending promptForSecret signal w/ msg" << msg;
        outputStream << QStringLiteral("promptForSecret") << msg;
    });
    QObject::connect(authenticators, &PamAuthenticators::infoMessage, &app, [&](const QString &msg) {
        qCDebug(TUTAMEN_HELPER) << "Helper is sending infoMessage signal for primary w/ msg" << msg;
        outputStream << QStringLiteral("infoMessage") << Primary << msg;
    });
    QObject::connect(authenticators,
                     &PamAuthenticators::noninteractiveInfo,
                     &app,
                     [&](PamAuthenticator::NoninteractiveAuthenticatorTypes what, PamAuthenticator *authenticator) {
                         qCDebug(TUTAMEN_HELPER) << "Helper is sending infoMessage signal for" << what << "w/ msg";
                         outputStream << QStringLiteral("infoMessage") << what << authenticator->getInfoMessage();
                     });
    QObject::connect(authenticators, &PamAuthenticators::errorMessage, &app, [&](const QString &msg) {
        qCDebug(TUTAMEN_HELPER) << "Helper is sending errorMessage signal for primary w/ msg" << msg;
        outputStream << QStringLiteral("errorMessage") << Primary << msg;
    });
    QObject::connect(authenticators,
                     &PamAuthenticators::noninteractiveError,
                     &app,
                     [&](PamAuthenticator::NoninteractiveAuthenticatorTypes what, PamAuthenticator *authenticator) {
                         qCDebug(TUTAMEN_HELPER) << "Helper is sending errorMessage signal for" << what << "w/ msg" << authenticator->getErrorMessage();
                         outputStream << QStringLiteral("errorMessage") << what << authenticator->getErrorMessage();
                     });
    QObject::connect(authenticators, &PamAuthenticators::authenticatorTypesChanged, &app, [&] {
        qCDebug(TUTAMEN_HELPER) << "Helper is sending authenticatorTypes signal w/" << authenticators->authenticatorTypes();
        outputStream << QStringLiteral("authenticatorTypes") << authenticators->authenticatorTypes();
    });
    QObject::connect(authenticators, &PamAuthenticators::failed, &app, [&](PamAuthenticator::NoninteractiveAuthenticatorTypes what, PamAuthenticator *authenticator) {
        Q_UNUSED(authenticator)

        qCDebug(TUTAMEN_HELPER) << "Helper is sending failed signal";
        outputStream << QStringLiteral("failed") << what;
    });
    QObject::connect(authenticators, &PamAuthenticators::succeeded, [&] {
        auto authority = PolkitQt1::Authority::instance();
        auto ok = authority->authenticationAgentResponseSync(cookie, identity);
        if (ok) {
            qCDebug(TUTAMEN_HELPER) << "Helper is sending success signal";
            outputStream << QStringLiteral("success");
        } else {
            qCWarning(TUTAMEN_HELPER) << "Failed to OK agent response";
            app.exit();
        }
    });

    qCDebug(TUTAMEN_HELPER) << "Helper is ready and listening to messages";
    return app.exec();
}