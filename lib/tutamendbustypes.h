#pragma once

#include <PolkitQt1/Identity>
#include <PolkitQt1/Subject>
#include <QDBusArgument>
#include <QString>

namespace Tutamen
{

struct DBusSubject {
    PolkitQt1::UnixSessionSubject subject;

    DBusSubject();
    DBusSubject(const PolkitQt1::UnixSessionSubject &subject);
    ~DBusSubject();
};

struct DBusIdentity {
    PolkitQt1::UnixUserIdentity identity;

    DBusIdentity();
    ~DBusIdentity();
};

using StringMap = QMap<QString, QString>;

}

QDBusArgument &operator<<(QDBusArgument &argument, const Tutamen::DBusSubject &subject);
const QDBusArgument &operator>>(const QDBusArgument &argument, Tutamen::DBusSubject &myStruct);

QDBusArgument &operator<<(QDBusArgument &argument, const Tutamen::DBusIdentity &subject);
const QDBusArgument &operator>>(const QDBusArgument &argument, Tutamen::DBusIdentity &myStruct);

Q_DECLARE_METATYPE(Tutamen::DBusSubject)
Q_DECLARE_METATYPE(Tutamen::DBusIdentity)
Q_DECLARE_METATYPE(Tutamen::StringMap)
