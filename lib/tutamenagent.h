#pragma once

#include <QDBusContext>
#include <QObject>

#include "tutamen_export.h"
#include "tutamendbustypes.h"

namespace Tutamen
{

class Session;

class TUTAMEN_EXPORT Agent final : public QObject, protected QDBusContext
{
    Q_OBJECT

    struct Private;
    QScopedPointer<Private> d;

public:
    explicit Agent(QObject *parent = nullptr);
    ~Agent();

    bool registerConnection();
    Q_SIGNAL void sessionStarted(Session *session);

public Q_SLOTS:
    void BeginAuthentication(const QString &action_id,
                             const QString &message,
                             const QString &icon_name,
                             Tutamen::StringMap details,
                             const QString &cookie,
                             const QList<Tutamen::DBusIdentity> &identities);
    void CancelAuthentication(const QString &cookie);
};

}
