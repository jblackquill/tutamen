#pragma once

#include <QObject>

#include <PolkitQt1/Identity>

#include "tutamen_export.h"
#include "tutamendbustypes.h"

namespace Tutamen
{

class Agent;
class Authenticators;

class TUTAMEN_EXPORT Session final : public QObject
{
    Q_OBJECT

    struct Private;
    QScopedPointer<Private> d;

    friend class Agent;
    explicit Session(const QString &actionID,
                     const QString &message,
                     const QString &iconName,
                     const Tutamen::StringMap &details,
                     const QList<PolkitQt1::UnixUserIdentity> &identities,
                     const QString &cookie,
                     Agent *parent);
    ~Session();

    Q_SIGNAL void success();
    Q_SIGNAL void cancelled();
    Q_SIGNAL void errored();

public:
    Q_SIGNAL void ended();
    Q_INVOKABLE void cancel();

    Q_PROPERTY(Authenticators *authenticators READ authenticators NOTIFY authenticatorsChanged)
    Authenticators *authenticators() const;
    Q_SIGNAL void authenticatorsChanged();

    Q_PROPERTY(QString actionID READ actionID CONSTANT)
    QString actionID() const;

    Q_PROPERTY(QString message READ message CONSTANT)
    QString message() const;

    Q_PROPERTY(QString iconName READ iconName CONSTANT)
    QString iconName() const;

    Q_PROPERTY(Tutamen::StringMap details READ details CONSTANT)
    Tutamen::StringMap details() const;

    Q_PROPERTY(QList<PolkitQt1::UnixUserIdentity> identities READ identities CONSTANT)
    QList<PolkitQt1::UnixUserIdentity> identities() const;
};

}
