#include <PolkitQt1/Authority>
#include <PolkitQt1/Identity>

#include <optional>

#include "authenticationagentadaptor.h"

#include "authorityinterface.h"
#include "tutamen_client_logging.h"
#include "tutamenagent.h"
#include "tutamensession.h"

namespace Tutamen
{

struct Agent::Private {
    QMap<QString, Session *> sessions;
    OrgFreedesktopPolicyKit1AuthorityInterface *interface;
};

static bool s_registeredTypes;

Agent::Agent(QObject *parent)
    : QObject(parent)
    , d(new Private)
{
    (void)new AuthenticationAgentAdaptor(this);
    d->interface = new OrgFreedesktopPolicyKit1AuthorityInterface(QStringLiteral("org.freedesktop.PolicyKit1"),
                                                                  QStringLiteral("/org/freedesktop/PolicyKit1/Authority"),
                                                                  QDBusConnection::systemBus(),
                                                                  this);
    if (!s_registeredTypes) {
        s_registeredTypes = true;
        qDBusRegisterMetaType<DBusSubject>();
        qDBusRegisterMetaType<DBusIdentity>();
        qDBusRegisterMetaType<QList<DBusIdentity>>();
        qRegisterMetaType<StringMap>("Tutamen::StringMap");
        qDBusRegisterMetaType<StringMap>();
    }
}

Agent::~Agent()
{
}

bool Agent::registerConnection()
{
    if (!QDBusConnection::systemBus().registerObject(QStringLiteral("/org/freedesktop/PolicyKit1/AuthenticationAgent"),
                                                     QStringLiteral("org.freedesktop.PolicyKit1.AuthenticationAgent"),
                                                     this)) {
        qCWarning(TUTAMEN_CLIENT) << "Agent: failed to register object at /org/freedesktop/PolicyKit1/AuthenticationAgent";
        return false;
    }

    PolkitQt1::UnixSessionSubject session(getpid());

    auto resp = d->interface->RegisterAuthenticationAgent(session, QLocale::system().name(), QStringLiteral("/org/freedesktop/PolicyKit1/AuthenticationAgent"));
    resp.waitForFinished();
    if (resp.isError()) {
        qCWarning(TUTAMEN_CLIENT) << "Agent: failed to register self with authority:" << resp.error();
        return false;
    }

    qCDebug(TUTAMEN_CLIENT) << "Agent: successfully registered self with authority";
    return true;
}

QList<PolkitQt1::UnixUserIdentity> findUserIdentity(const QList<Tutamen::DBusIdentity> &identities)
{
    QList<PolkitQt1::UnixUserIdentity> ret;
    for (const auto &identity : identities) {
        if (identity.identity.isValid()) {
            ret.append(identity.identity);
        }
    }
    return ret;
}

void Agent::BeginAuthentication(const QString &action_id,
                                const QString &message,
                                const QString &icon_name,
                                Tutamen::StringMap details,
                                const QString &cookie,
                                const QList<Tutamen::DBusIdentity> &identities)
{
    setDelayedReply(true);
    qCDebug(TUTAMEN_CLIENT) << "Agent:" << qUtf8Printable(cookie) << "received begin authentication request from authority";

    const auto msg = this->message();
    const auto conn = this->connection();
    const auto userIdentities = findUserIdentity(identities);

    if (userIdentities.isEmpty()) {
        qCWarning(TUTAMEN_CLIENT) << "Agent: could not find supported identity type";
        auto reply = msg.createErrorReply(QStringLiteral("org.freedesktop.PolicyKit1.Error.Failed"), QString());
        conn.send(reply);
        return;
    }

    auto session = d->sessions[cookie] = new Session(action_id, message, icon_name, details, userIdentities, cookie, this);
    qCDebug(TUTAMEN_CLIENT) << "Agent:" << qUtf8Printable(cookie) << "created new session" << session;

    connect(session, &Session::success, this, [=] {
        qCDebug(TUTAMEN_CLIENT) << "Agent:" << qUtf8Printable(cookie)
                                << "session was a success, sending void reply to begin authentication request and removing session";
        auto reply = msg.createReply();
        if (!conn.send(reply)) {
            qCWarning(TUTAMEN_CLIENT) << "Agent:" << qUtf8Printable(cookie) << "failed to send void reply to begin authentication request";
        }
        d->sessions.remove(cookie);
        Q_EMIT session->ended();
    });
    connect(session, &Session::cancelled, this, [=] {
        qCDebug(TUTAMEN_CLIENT) << "Agent:" << qUtf8Printable(cookie)
                                << "session was cancelled, sending error reply to begin authentication request and removing session";
        auto reply = msg.createErrorReply(QStringLiteral("org.freedesktop.PolicyKit1.Error.Cancelled"), QString());
        if (!conn.send(reply)) {
            qCWarning(TUTAMEN_CLIENT) << "Agent:" << qUtf8Printable(cookie) << "failed to send error reply to begin authentication request";
        }
        d->sessions.remove(cookie);
        Q_EMIT session->ended();
    });
    connect(session, &Session::errored, this, [=] {
        qCDebug(TUTAMEN_CLIENT) << "Agent:" << qUtf8Printable(cookie)
                                << "session errored out, sending error reply to begin authentication request and removing session";
        auto reply = msg.createErrorReply(QStringLiteral("org.freedesktop.PolicyKit1.Error.Failed"), QString());
        if (!conn.send(reply)) {
            qCWarning(TUTAMEN_CLIENT) << "Agent:" << qUtf8Printable(cookie) << "failed to send error reply to begin authentication request";
        }
        d->sessions.remove(cookie);
        Q_EMIT session->ended();
    });

    qCDebug(TUTAMEN_CLIENT) << "Agent: notifiying listeners of new session";
    Q_EMIT sessionStarted(session);
}

void Agent::CancelAuthentication(const QString &cookie)
{
    qCDebug(TUTAMEN_CLIENT) << "Agent: received cancellation request from authority for" << cookie;
    auto session = d->sessions.value(cookie);
    if (session == nullptr) {
        qCWarning(TUTAMEN_CLIENT) << "Agent: could not find" << cookie << "in our sessions, despite authority asking to cancel";
        return;
    }

    session->cancel();
}

}
