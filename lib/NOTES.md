# Using polkit-qt-1

Don't use polkit-qt-1's DBus methods; they use a separate connection maintained by GLib which confuses PolKit when we're using the connection maintained by Qt
