#include <PolkitQt1/Identity>
#include <QProcess>

#include "tutamen_client_logging.h"
#include "tutamen_helper_location.h"
#include "tutamenauthenticators.h"

namespace Tutamen
{

struct Authenticators::Private {
    QProcess helperProcess;
    QString cookie;
    AuthenticatorsState state = AuthenticatorsState::Idle;
    QString prompt;
    QString secretPrompt;
    QMap<AuthenticatorKind, QString> infoMessages;
    QMap<AuthenticatorKind, QString> errorMessages;
    AuthenticatorKind authenticatorTypes = AuthenticatorKind::Primary;

    auto debug()
    {
        return qUtf8Printable(QStringLiteral("Authenticators ") + cookie + QChar::fromLatin1(':'));
    }
};

Authenticators::Authenticators(const PolkitQt1::UnixUserIdentity &identity, const QString &cookie, QObject *parent)
    : QObject(parent)
    , d(new Private)
{
    connect(&d->helperProcess, &QProcess::readyReadStandardOutput, this, &Authenticators::processReadyRead);
    connect(&d->helperProcess, &QProcess::finished, this, &Authenticators::processFinished);
    connect(&d->helperProcess, &QProcess::stateChanged, this, &Authenticators::processStateChanged);
    connect(&d->helperProcess, &QProcess::errorOccurred, this, &Authenticators::processErrorOccurred);
    d->cookie = cookie;

    d->helperProcess.setProcessChannelMode(QProcess::ForwardedErrorChannel);
    qCDebug(TUTAMEN_CLIENT) << d->debug() << "starting helper process at" << TUTAMEN_HELPER_LOCATION;
    d->helperProcess.start(QStringLiteral(TUTAMEN_HELPER_LOCATION), {identity.toString()}, QIODeviceBase::ReadWrite);
    d->helperProcess.write(cookie.toLocal8Bit() + '\n');
}

Authenticators::~Authenticators()
{
}

/*
$$$$$$$\                                $$\            $$\                            $$$$$$\  $$\                               $$\
$$  __$$\                               \__|           \__|                          $$  __$$\ \__|                              $$ |
$$ |  $$ | $$$$$$\   $$$$$$$\  $$$$$$\  $$\ $$\    $$\ $$\ $$$$$$$\   $$$$$$\        $$ /  \__|$$\  $$$$$$\  $$$$$$$\   $$$$$$\  $$ | $$$$$$$\
$$$$$$$  |$$  __$$\ $$  _____|$$  __$$\ $$ |\$$\  $$  |$$ |$$  __$$\ $$  __$$\       \$$$$$$\  $$ |$$  __$$\ $$  __$$\  \____$$\ $$ |$$  _____|
$$  __$$< $$$$$$$$ |$$ /      $$$$$$$$ |$$ | \$$\$$  / $$ |$$ |  $$ |$$ /  $$ |       \____$$\ $$ |$$ /  $$ |$$ |  $$ | $$$$$$$ |$$ |\$$$$$$\
$$ |  $$ |$$   ____|$$ |      $$   ____|$$ |  \$$$  /  $$ |$$ |  $$ |$$ |  $$ |      $$\   $$ |$$ |$$ |  $$ |$$ |  $$ |$$  __$$ |$$ | \____$$\
$$ |  $$ |\$$$$$$$\ \$$$$$$$\ \$$$$$$$\ $$ |   \$  /   $$ |$$ |  $$ |\$$$$$$$ |      \$$$$$$  |$$ |\$$$$$$$ |$$ |  $$ |\$$$$$$$ |$$ |$$$$$$$  |
\__|  \__| \_______| \_______| \_______|\__|    \_/    \__|\__|  \__| \____$$ |       \______/ \__| \____$$ |\__|  \__| \_______|\__|\_______/
                                                                     $$\   $$ |                    $$\   $$ |
                                                                     \$$$$$$  |                    \$$$$$$  |
                                                                      \______/                      \______/
*/

void Authenticators::processStateChanged()
{
    qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper process state changed to" << d->helperProcess.state();
}

void Authenticators::processFinished()
{
    qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper process finished";
}

void Authenticators::processErrorOccurred(QProcess::ProcessError error)
{
    qCWarning(TUTAMEN_CLIENT) << d->debug() << "helper process errored with" << error;
    Q_EMIT errored();
}

void Authenticators::processReadyRead()
{
    qCDebug(TUTAMEN_CLIENT) << d->debug() << "got ready read signal from the helper process";
    while (d->helperProcess.bytesAvailable() > 0) {
        qCDebug(TUTAMEN_CLIENT) << d->debug() << "reading message from helper process";
        QDataStream reader(&d->helperProcess);
        QString signal;

        reader >> signal;
        qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper sent signal of type" << signal;

        if (signal == QStringLiteral("state")) {
            AuthenticatorsState state;
            reader >> state;
            qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper sent state" << state;

            if (d->state == state) {
                return;
            }
            d->state = state;
            Q_EMIT stateChanged();
        } else if (signal == QStringLiteral("prompt")) {
            QString prompt;
            reader >> prompt;
            qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper sent prompt" << prompt;

            if (d->prompt == prompt) {
                return;
            }
            d->prompt = prompt;
            Q_EMIT promptChanged();
        } else if (signal == QStringLiteral("promptForSecret")) {
            QString prompt;
            reader >> prompt;
            qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper sent prompt for secret" << prompt;

            if (d->secretPrompt == prompt) {
                return;
            }
            d->secretPrompt = prompt;
            Q_EMIT secretPromptChanged();
        } else if (signal == QStringLiteral("infoMessage")) {
            AuthenticatorKind authenticator;
            QString message;
            reader >> authenticator >> message;
            qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper sent info message" << authenticator << message;

            d->infoMessages[authenticator] = message;
            Q_EMIT infoMessagesChanged(authenticator);
        } else if (signal == QStringLiteral("errorMessage")) {
            AuthenticatorKind authenticator;
            QString message;
            reader >> authenticator >> message;
            qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper sent error message" << authenticator << message;

            d->errorMessages[authenticator] = message;
            Q_EMIT errorMessagesChanged(authenticator);
        } else if (signal == QStringLiteral("authenticatorTypes")) {
            AuthenticatorKind types;
            reader >> types;
            qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper sent authenticator types" << types;

            if (d->authenticatorTypes == types) {
                return;
            }
            d->authenticatorTypes = types;
            Q_EMIT authenticatorTypesChanged();
        } else if (signal == QStringLiteral("failed")) {
            AuthenticatorKind types;
            reader >> types;
            qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper sent failed" << types;
            Q_EMIT failed(types);
        } else if (signal == QStringLiteral("success")) {
            qCDebug(TUTAMEN_CLIENT) << d->debug() << "helper sent success";
            Q_EMIT succeeded();
        } else {
            qCWarning(TUTAMEN_CLIENT) << "Unknown signal" << signal;
        }
    }
}

/*
 $$$$$$\                            $$\ $$\                            $$$$$$\  $$\                               $$\
$$  __$$\                           $$ |\__|                          $$  __$$\ \__|                              $$ |
$$ /  \__| $$$$$$\  $$$$$$$\   $$$$$$$ |$$\ $$$$$$$\   $$$$$$\        $$ /  \__|$$\  $$$$$$\  $$$$$$$\   $$$$$$\  $$ | $$$$$$$\
\$$$$$$\  $$  __$$\ $$  __$$\ $$  __$$ |$$ |$$  __$$\ $$  __$$\       \$$$$$$\  $$ |$$  __$$\ $$  __$$\  \____$$\ $$ |$$  _____|
 \____$$\ $$$$$$$$ |$$ |  $$ |$$ /  $$ |$$ |$$ |  $$ |$$ /  $$ |       \____$$\ $$ |$$ /  $$ |$$ |  $$ | $$$$$$$ |$$ |\$$$$$$\
$$\   $$ |$$   ____|$$ |  $$ |$$ |  $$ |$$ |$$ |  $$ |$$ |  $$ |      $$\   $$ |$$ |$$ |  $$ |$$ |  $$ |$$  __$$ |$$ | \____$$\
\$$$$$$  |\$$$$$$$\ $$ |  $$ |\$$$$$$$ |$$ |$$ |  $$ |\$$$$$$$ |      \$$$$$$  |$$ |\$$$$$$$ |$$ |  $$ |\$$$$$$$ |$$ |$$$$$$$  |
 \______/  \_______|\__|  \__| \_______|\__|\__|  \__| \____$$ |       \______/ \__| \____$$ |\__|  \__| \_______|\__|\_______/
                                                      $$\   $$ |                    $$\   $$ |
                                                      \$$$$$$  |                    \$$$$$$  |
                                                       \______/                      \______/
*/

void Authenticators::startAuthenticating()
{
    qCDebug(TUTAMEN_CLIENT) << d->debug() << "sending startAuthenticating to helper";
    QDataStream writer(&d->helperProcess);
    writer << QStringLiteral("startAuthenticating");
}

void Authenticators::stopAuthenticating()
{
    qCDebug(TUTAMEN_CLIENT) << d->debug() << "sending stopAuthenticating to helper";
    QDataStream writer(&d->helperProcess);
    writer << QStringLiteral("stopAuthenticating");
}

void Authenticators::respond(const QByteArray &answer)
{
    qCDebug(TUTAMEN_CLIENT) << d->debug() << "sending respond to helper";
    QDataStream writer(&d->helperProcess);
    writer << QStringLiteral("respond") << answer;
}

void Authenticators::respondString(const QString &answer)
{
    respond(answer.toLocal8Bit());
}

/*
 $$$$$$\             $$\     $$\
$$  __$$\            $$ |    $$ |
$$ /  \__| $$$$$$\ $$$$$$\ $$$$$$\    $$$$$$\   $$$$$$\   $$$$$$$\
$$ |$$$$\ $$  __$$\\_$$  _|\_$$  _|  $$  __$$\ $$  __$$\ $$  _____|
$$ |\_$$ |$$$$$$$$ | $$ |    $$ |    $$$$$$$$ |$$ |  \__|\$$$$$$\
$$ |  $$ |$$   ____| $$ |$$\ $$ |$$\ $$   ____|$$ |       \____$$\
\$$$$$$  |\$$$$$$$\  \$$$$  |\$$$$  |\$$$$$$$\ $$ |      $$$$$$$  |
 \______/  \_______|  \____/  \____/  \_______|\__|      \_______/
*/

Authenticators::AuthenticatorsState Authenticators::state() const
{
    return d->state;
}

Authenticators::AuthenticatorKind Authenticators::authenticatorTypes() const
{
    return d->authenticatorTypes;
}

QString Authenticators::prompt() const
{
    return d->prompt;
}

QString Authenticators::secretPrompt() const
{
    return d->secretPrompt;
}

QMap<Authenticators::AuthenticatorKind, QString> Authenticators::infoMessages() const
{
    return d->infoMessages;
}

QMap<Authenticators::AuthenticatorKind, QString> Authenticators::errorMessages() const
{
    return d->errorMessages;
}

}
