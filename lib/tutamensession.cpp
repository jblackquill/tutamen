#include "tutamensession.h"
#include "tutamen_client_logging.h"
#include "tutamenagent.h"
#include "tutamenauthenticators.h"

namespace Tutamen
{

struct Session::Private {
    QString actionID;
    QString message;
    QString iconName;
    StringMap details;
    QList<PolkitQt1::UnixUserIdentity> identities;
    Authenticators *authenticators;
    QString cookie;

    auto debug()
    {
        return qUtf8Printable(QStringLiteral("Session: ") + cookie + QChar::fromLatin1(':'));
    }
};

Session::Session(const QString &actionID,
                 const QString &message,
                 const QString &iconName,
                 const Tutamen::StringMap &details,
                 const QList<PolkitQt1::UnixUserIdentity> &identities,
                 const QString &cookie,
                 Agent *parent)
    : QObject(parent)
    , d(new Private)
{
    d->actionID = actionID;
    d->message = message;
    d->iconName = iconName;
    d->details = details;
    d->cookie = cookie;
    d->identities = identities;
    d->authenticators = new Authenticators(identities.first(), cookie, this);

    connect(d->authenticators, &Authenticators::succeeded, this, &Session::success);
    connect(d->authenticators, &Authenticators::errored, this, &Session::errored);
    connect(this, &Session::ended, this, &QObject::deleteLater);
}

Session::~Session()
{
}

void Session::cancel()
{
    Q_EMIT cancelled();
}

Authenticators *Session::authenticators() const
{
    return d->authenticators;
}

QString Session::actionID() const
{
    return d->actionID;
}

QString Session::message() const
{
    return d->message;
}

QString Session::iconName() const
{
    return d->iconName;
}

Tutamen::StringMap Session::details() const
{
    return d->details;
}

QList<PolkitQt1::UnixUserIdentity> Session::identities() const
{
    return d->identities;
}
}
