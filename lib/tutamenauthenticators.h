#pragma once

#include <PolkitQt1/Identity>
#include <QMap>
#include <QObject>
#include <QProcess>

#include "tutamen_export.h"

namespace Tutamen
{

class TUTAMEN_EXPORT Authenticators final : public QObject
{
    Q_OBJECT

    struct Private;
    QScopedPointer<Private> d;

    void processReadyRead();
    void processFinished();
    void processErrorOccurred(QProcess::ProcessError error);
    void processStateChanged();

public:
    explicit Authenticators(const PolkitQt1::UnixUserIdentity &user, const QString &cookie, QObject *parent = nullptr);
    ~Authenticators();

    enum AuthenticatorKind : uint32_t {
        Primary = 0,
        Fingerprint = 1 << 0,
        Smartcard = 2 << 0,
    };
    Q_DECLARE_FLAGS(AuthenticatorKinds, AuthenticatorKind)
    Q_FLAG(AuthenticatorKinds)

    enum AuthenticatorsState : uint32_t {
        Idle = 0,
        Authenticating = 1,
        Failed = 2,
    };
    Q_ENUM(AuthenticatorsState)

    Q_INVOKABLE void startAuthenticating();
    Q_INVOKABLE void stopAuthenticating();
    Q_INVOKABLE void respond(const QByteArray &answer);
    Q_INVOKABLE void respondString(const QString &answer);

    Q_SIGNAL void failed(AuthenticatorKind which);
    Q_SIGNAL void succeeded();
    Q_SIGNAL void errored();

    Q_PROPERTY(AuthenticatorsState state READ state NOTIFY stateChanged)
    AuthenticatorsState state() const;
    Q_SIGNAL void stateChanged();

    Q_PROPERTY(AuthenticatorKind authenticatorTypes READ authenticatorTypes NOTIFY authenticatorTypesChanged)
    AuthenticatorKind authenticatorTypes() const;
    Q_SIGNAL void authenticatorTypesChanged();

    Q_PROPERTY(QString prompt READ prompt NOTIFY promptChanged)
    QString prompt() const;
    Q_SIGNAL void promptChanged();

    Q_PROPERTY(QString secretPrompt READ secretPrompt NOTIFY secretPromptChanged)
    QString secretPrompt() const;
    Q_SIGNAL void secretPromptChanged();

    Q_PROPERTY(QMap<AuthenticatorKind, QString> infoMessages READ infoMessages NOTIFY infoMessagesChanged)
    QMap<AuthenticatorKind, QString> infoMessages() const;
    Q_SIGNAL void infoMessagesChanged(AuthenticatorKind which);

    Q_PROPERTY(QMap<AuthenticatorKind, QString> errorMessages READ errorMessages NOTIFY errorMessagesChanged)
    QMap<AuthenticatorKind, QString> errorMessages() const;
    Q_SIGNAL void errorMessagesChanged(AuthenticatorKind which);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Authenticators::AuthenticatorKinds)
}
