#include "tutamendbustypes.h"
#include "tutamen_client_logging.h"

QDBusArgument &operator<<(QDBusArgument &argument, const Tutamen::DBusSubject &subject)
{
    argument.beginStructure();
    argument << QStringLiteral("unix-session");
    argument.beginMap(QMetaType::fromType<QString>(), QMetaType::fromType<QDBusVariant>());
    argument.beginMapEntry();
    argument << QStringLiteral("session-id") << QDBusVariant(subject.subject.sessionId());
    argument.endMapEntry();
    argument.endMap();
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, Tutamen::DBusSubject &myStruct)
{
    Q_UNUSED(myStruct)
    qCWarning(TUTAMEN_CLIENT) << "Should not be reading a Subject; we should only be writing subjects in the RegisterAuthenticationAgent method!";
    return argument;
}

Tutamen::DBusSubject::DBusSubject()
    : subject(PolkitQt1::UnixSessionSubject(getpid()))
{
}

Tutamen::DBusSubject::DBusSubject(const PolkitQt1::UnixSessionSubject &subject)
    : subject(subject)
{
}

Tutamen::DBusSubject::~DBusSubject()
{
}

Tutamen::DBusIdentity::DBusIdentity()
{
}

Tutamen::DBusIdentity::~DBusIdentity()
{
}

QDBusArgument &operator<<(QDBusArgument &argument, const Tutamen::DBusIdentity &identity)
{
    argument.beginStructure();
    argument << QStringLiteral("unix-user");
    argument.beginMap(QMetaType::fromType<QString>(), QMetaType::fromType<QDBusVariant>());
    argument.beginMapEntry();
    argument << QStringLiteral("uid") << QDBusVariant(identity.identity.uid());
    argument.endMapEntry();
    argument.endMap();
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, Tutamen::DBusIdentity &identity)
{
    argument.beginStructure();
    QString kind;
    argument >> kind;
    argument.beginMap();
    while (!argument.atEnd()) {
        argument.beginMapEntry();
        QString key;
        QDBusVariant uid;
        argument >> key >> uid;
        if (kind == QStringLiteral("unix-user") && key == QStringLiteral("uid")) {
            identity.identity = PolkitQt1::UnixUserIdentity(uid.variant().toUInt());
        }
        argument.endMapEntry();
    }
    argument.endMap();
    argument.endStructure();
    return argument;
}
